<?php

namespace App\BookTitle;
use App\Model\Database as DB;

class BookTitle extends DB
{

    private $id;
    private $book_name;
    private $author_name;

    public function setData($postData){
        if(array_key_exists('id',$postData)){

            $this->id = $postData['id'];

        }
        if(array_key_exists('bookName',$postData)){

            $this->book_name = $postData['bookName'];

        }
        if(array_key_exists('authorName',$postData)){

            $this->author_name = $postData['authorName'];

        }
    }

    public function store(){

        $arrData = array($this->book_name,$this->author_name);

        $sql = "INSERT into book_title(book_name,author_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        Utility::redirect('create.php');

    }

    public function index(){
        echo "I'm inside:".__METHOD__;
    }

}